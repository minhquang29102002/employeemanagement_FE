import { DatePipe, registerLocaleData } from '@angular/common';
import vi from '@angular/common/locales/vi';
import { LOCALE_ID, NgModule } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconModule } from '@ant-design/icons-angular';
import { OAuthModule, OAuthStorage } from 'angular-oauth2-oidc';
import { NZ_I18N, vi_VN } from 'ng-zorro-antd/i18n';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './core/shared/shared.module';
import localeVi from '@angular/common/locales/vi';
registerLocaleData(localeVi);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    OAuthModule.forRoot(),
    AppRoutingModule,
    IconModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: vi_VN },
    {
      provide: OAuthStorage,
      useValue: localStorage,
    },
    { provide: LOCALE_ID, useValue: 'vi-VN' },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
