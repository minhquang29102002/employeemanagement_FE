import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { SettingsRoutes } from './settings.routing';
import { ChangePwComponent } from './change-pw/change-pw.component';
import { InfomationComponent } from './infomation/infomation.component';
import { SidebarSettingComponent } from './sidebar-setting/sidebar-setting.component';

@NgModule({
  imports: [CommonModule, SettingsRoutes, SharedModule],
  declarations: [SettingsComponent, ChangePwComponent, InfomationComponent, SidebarSettingComponent],
})
export class SettingsModule {}
