import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { appearance } from 'src/app/core/const/material';
import { Position, Positions } from 'src/app/core/const/position';
import { Department } from 'src/app/core/models/department';
import { Information } from 'src/app/core/models/information';
import { DepartmentService } from 'src/app/core/services/department.service';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { CommonService } from 'src/app/core/shared/services/common.service';

@Component({
  selector: 'app-infomation',
  templateUrl: './infomation.component.html',
  styleUrls: ['./infomation.component.scss'],
})
export class InfomationComponent implements OnInit, AfterViewInit {
  isEdit: boolean = false;
  infoView: View[] = [];
  information: Information = new Information();
  appearance = appearance;
  Positions = Positions;
  form: FormGroup = this.fb.group({
    fullname: '',
    gender: '',
    dob: '',
    phone: '',
    email: '',
    cccd: '',
    address: '',
    department: '',
    role: '',
    position: '',
  });
  departmentList: Department[] = [];
  constructor(
    private commonService: CommonService,
    private fb: FormBuilder,
    private employeeService: EmployeeService,
    private noti: NotiService,
    private datepipe: DatePipe,
    private departmentService: DepartmentService
  ) {}
  ngAfterViewInit(): void {}

  ngOnInit(): void {
    this.mapData();
    this.getAllDepartment();
  }

  getAllDepartment() {
    this.departmentService.getAll('All').subscribe((data) => {
      this.departmentList = data;
    });
  }

  mapData() {
    this.commonService.information$.subscribe((data) => {
      this.information = data;
      let gender: string = '';
      switch (data.gender) {
        case 0: {
          gender = 'Nữ';
          break;
        }
        case 1: {
          gender = 'Nam';
          break;
        }
        case 2: {
          gender = 'Khác';
          break;
        }
      }
      let position: Position = new Position();
      if (data.position) {
        position = Positions.find((p) => p.value === data.position)!;
      }
      this.infoView = [
        { label: 'Họ và tên', value: data.fullName },
        { label: 'Giới tính', value: gender },
        {
          label: 'Ngày sinh',
          value: this.datepipe.transform(data.dob, 'dd/MM/yyyy'),
        },
        { label: 'Số điện thoại', value: data.phone },
        { label: 'Email', value: data.email },
        { label: 'Số CMND/CCCD', value: data.cccd },
        { label: 'Địa chỉ liên hệ', value: data.address },
        { label: 'Phòng ban', value: data.department?.name },
        {
          label: 'Vai trò trong hệ thống',
          value: data.role === 0 ? 'Admin' : 'User',
        },
        {
          label: 'Vị trí công việc',
          value: position.text,
        },
      ];
      this.patchValueToForm();
    });
  }

  patchValueToForm() {
    this.form.patchValue({
      fullname: this.information.fullName,
      gender: this.information.gender,
      dob: this.information.dob,
      phone: this.information.phone,
      email: this.information.email,
      cccd: this.information.cccd,
      address: this.information.address,
      department: this.information.department?.departmentId,
      role: this.information.role == 0 ? 'Admin' : 'User',
      position: this.information.position,
    });
  }

  edit() {
    this.isEdit = true;
    this.configPadding();
  }

  onChange(result: any): void {
    console.log('onChange: ', result);
  }

  configPadding() {
    setTimeout(() => {
      const childElement = document.getElementById('datepicker');
      const parentEle = childElement?.closest(
        '.mat-form-field-infix'
      ) as HTMLElement;
      parentEle.style.padding = '0px';
    });
  }

  save() {
    let body = {
      employeeId: this.information.employeeId,
      fullName: this.form.get('fullname')?.value,
      gender: this.form.get('gender')?.value,
      dob: this.form.get('dob')?.value
        ? new Date(
            moment(this.form.get('dob')?.value).format('YYYY-MM-DD')
          ).toISOString()
        : null,
      phone: this.form.get('phone')?.value,
      cccd: this.form.get('cccd')?.value,
      address: this.form.get('address')?.value,
      departmentId: this.form.get('department')?.value,
      position: this.form.get('position')?.value,
      email: this.form.get('email')?.value,
    };
    this.employeeService.update(body).subscribe(
      (data) => {},
      (err) => {
        this.noti.warningNoti();
        this.isEdit = false;
      },
      () => {
        this.noti.successNoti();
        this.commonService.getInfomation();
        this.isEdit = false;
      }
    );
  }
}

export class View {
  label?: string;
  value?: string;
}
