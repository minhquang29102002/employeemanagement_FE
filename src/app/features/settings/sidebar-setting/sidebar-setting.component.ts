import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar-setting',
  templateUrl: './sidebar-setting.component.html',
  styleUrls: ['./sidebar-setting.component.scss'],
})
export class SidebarSettingComponent implements OnInit {
  Menu: Sidebar[] = [
    {
      text: 'Thông tin cá nhân',
      routerLink: '/main/setting/info',
      icon: '<i class="fa-regular fa-id-badge"></i>',
    },
    {
      text: 'Đổi mật khẩu',
      routerLink: '/main/setting/change-pw',
      icon: '<i class="fa-solid fa-user-lock"></i>',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}

export class Sidebar {
  text?: string;
  routerLink?: string;
  icon?: string;
}
