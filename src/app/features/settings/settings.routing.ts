import { Routes, RouterModule } from '@angular/router';
import { ChangePwComponent } from './change-pw/change-pw.component';
import { InfomationComponent } from './infomation/infomation.component';
import { SettingsComponent } from './settings.component';

const routes: Routes = [
  {
    path: '',
    component: SettingsComponent,
    children: [
      {
        path: 'info',
        component: InfomationComponent,
      },
      {
        path: 'change-pw',
        component: ChangePwComponent,
      },
    ],
  },
];

export const SettingsRoutes = RouterModule.forChild(routes);
