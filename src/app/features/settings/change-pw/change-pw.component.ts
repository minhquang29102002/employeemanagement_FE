import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { appearance } from 'src/app/core/const/material';
import { AccountService } from 'src/app/core/services/account.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { ValidatorService } from 'src/app/core/shared/validators/validator.service';

@Component({
  selector: 'app-change-pw',
  templateUrl: './change-pw.component.html',
  styleUrls: ['./change-pw.component.scss'],
})
export class ChangePwComponent implements OnInit {
  form: FormGroup = this.fb.group(
    {
      currentPw: ['', Validators.required],
      newPw: ['', [Validators.required, Validators.minLength(6)]],
      reNewPw: ['', Validators.required],
    },
    {
      validators: [
        this.validatorService.matchPasswordsValidator('newPw', 'reNewPw'),
      ],
    }
  );
  appearance = appearance;
  constructor(
    private fb: FormBuilder,
    private noti: NotiService,
    private accountService: AccountService,
    private router: Router,
    private validatorService: ValidatorService
  ) {}

  ngOnInit(): void {}

  show(event: any) {
    let input = event.target
      .closest('.field')
      .querySelector('.input') as HTMLInputElement;
    input.setAttribute('type', 'text');
    let iconHide = event.target
      .closest('.field')
      .querySelector('.hide') as HTMLElement;
    let iconShow = event.target
      .closest('.field')
      .querySelector('.show') as HTMLElement;
    iconHide.classList.add('hidden');
    iconShow.classList.remove('hidden');
  }
  hide(event: any) {
    let input = event.target
      .closest('.field')
      .querySelector('.input') as HTMLInputElement;
    input.setAttribute('type', 'password');
    let iconHide = event.target
      .closest('.field')
      .querySelector('.hide') as HTMLElement;
    let iconShow = event.target
      .closest('.field')
      .querySelector('.show') as HTMLElement;
    iconHide.classList.remove('hidden');
    iconShow.classList.add('hidden');
  }

  save() {
    if (!this.form.valid) {
      if (this.form.get('newPw')?.value != this.form.get('reNewPw')?.value) {
        this.noti.warningNoti('Nhập lại chưa đúng mật khẩu');
      }
    } else {
      let body = {
        employeeId: localStorage.getItem('id'),
        password: this.form.get('currentPw')?.value,
        newPassWord: this.form.get('newPw')?.value,
      };
      this.accountService.changePw(body).subscribe(
        (data) => {},
        (err) => {
          this.noti.warningNoti(err.error);
        },
        () => {
          this.noti.successNoti(
            'Cập nhật mật khẩu thành công, mời bạn đăng nhập lại'
          );
          setTimeout(() => {
            this.router.navigate(['/auth']);
          }, 2000);
        }
      );
    }
  }
}
