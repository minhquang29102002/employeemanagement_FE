import { Component, Input, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NzModalService } from 'ng-zorro-antd/modal';
import { appearance } from 'src/app/core/const/material';
import { Positions } from 'src/app/core/const/position';
import { Department } from 'src/app/core/models/department';
import { Employee } from 'src/app/core/models/employee';
import { DepartmentService } from 'src/app/core/services/department.service';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { NotiService } from 'src/app/core/services/noti.service';

@Component({
  selector: 'app-view-edit',
  templateUrl: './view-edit.component.html',
  styleUrls: ['./view-edit.component.scss'],
})
export class ViewEditComponent implements OnInit {
  detailEmployee: Employee = new Employee();
  isEdit: boolean = false;
  title: string = 'Xem chi tiết nhân viên';
  form: FormGroup = this.fb.group({
    name: '',
    role: '',
    position: '',
    department: '',
  });
  appearance = appearance;
  Positions = Positions;
  departmentList: Department[] = [];
  roleList = [
    { value: 0, text: 'Admin' },
    { value: 1, text: 'User' },
  ];
  constructor(
    public dialogRef: MatDialogRef<ViewEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private employeeService: EmployeeService,
    private fb: FormBuilder,
    private departmentService: DepartmentService,
    private noti: NotiService,
    private nzModel: NzModalService
  ) {}

  ngOnInit(): void {
    this.getAllDepartment();
    this.getEmployeeById();
  }

  getEmployeeById() {
    this.employeeService.getById(this.data.id).subscribe((data: Employee) => {
      this.detailEmployee = data;
      this.form.patchValue({
        name: data.fullName,
        position: data.position,
        role: data.role,
        department: data.department?.departmentId,
      });
    });
  }

  getAllDepartment() {
    this.departmentService.getAll('All').subscribe((data) => {
      this.departmentList = data;
    });
  }

  close() {
    this.dialogRef.close(false);
  }
  save() {
    let body = {
      employeeId: this.data.id,
      role: this.form.get('role')?.value,
      departmentId: this.form.get('department')?.value,
      position: this.form.get('position')?.value,
    };
    this.employeeService.update(body).subscribe(
      (data) => {
        this.noti.successNoti();
        this.dialogRef.close(true);
      },
      (err) => {
        this.noti.warningNoti();
      }
    );
  }
  delete() {
    const confirm = this.nzModel.confirm({
      nzTitle: 'Bạn có muốn xóa nhân viên này không?',
      nzContent:
        'Nếu xóa nhân viên sẽ tự động được xóa khỏi phòng ban và tài khoản cũng tự động bị xóa khỏi hệ thống',
      nzOkText: 'Xóa',
      nzCancelText: 'Hủy',
      nzOnOk: () => {
        this.employeeService.delete(this.data.id).subscribe(
          (data) => {
            this.noti.successNoti('Xóa nhân viên thành công');
            this.dialogRef.close(true);
          },
          (err) => {
            this.noti.warningNoti();
          }
        );
      },
    });
  }
}
export enum TypeScreen {
  EDIT,
  VIEW,
}
