import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { EmployeeRoutes } from './employee.routing';
import { ViewEditComponent } from './view-edit/view-edit.component';
import { AddComponent } from './add/add.component';

@NgModule({
  imports: [CommonModule, EmployeeRoutes, SharedModule],
  declarations: [EmployeeComponent, ViewEditComponent, AddComponent],
})
export class EmployeeModule {}
