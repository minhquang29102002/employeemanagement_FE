import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Position, Positions } from 'src/app/core/const/position';
import { Employee } from 'src/app/core/models/employee';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { NotiService } from 'src/app/core/services/noti.service';
import {
  ListRow,
  Screen,
} from 'src/app/core/shared/components/table/table.component';
import { AddComponent } from './add/add.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  count: number = 0;
  headerList: string[] = [
    'STT',
    'Họ tên',
    'Ngày sinh',
    'Giới tính',
    'Email',
    'Điện thoại di động',
    'Phòng ban',
    'Vị trí',
    'Ngày vào làm',
    'Hành động',
  ];
  data: ListRow[] = [];
  Screen = Screen;
  constructor(
    private employeeService: EmployeeService,
    private datePipe: DatePipe,
    private modal: NzModalService,
    private noti: NotiService
  ) {}

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.employeeService.getAll('All').subscribe((data: Employee[]) => {
      if (data) {
        this.data = [];
        data.forEach((employee: Employee, index) => {
          var gender;
          switch (employee.gender) {
            case 0: {
              gender = 'Nữ';
              break;
            }
            case 1: {
              gender = 'Nam';
              break;
            }
            case 2: {
              gender = 'Khác';
              break;
            }
          }
          let position: Position = new Position();
          if (employee.position) {
            position = Positions.find((p) => p.value === employee.position)!;
          }
          this.data.push({
            id: employee.employeeId,
            listCol: [
              String(index + 1),
              employee.fullName || '',
              this.datePipe.transform(employee.dob, 'dd/MM/yyyy') || '',
              gender || '',
              employee.email || '',
              employee.phone || '',
              employee.department?.name || '',
              position.text,
              this.datePipe.transform(employee.startDate, 'dd/MM/yyyy') || '',
            ],
          });
        });
        this.count = data.length;
      }
    });
  }
  search($event: any) {
    var keyword = 'All';
    if (
      $event.target.value.trim() === '' ||
      $event.target.value.trim() === null
    ) {
      keyword = 'All';
    } else {
      keyword = $event.target.value.trim();
    }
    this.employeeService.getAll(keyword).subscribe((data: Employee[]) => {
      if (data) {
        this.data = [];
        data.forEach((employee: Employee, index) => {
          var gender;
          switch (employee.gender) {
            case 0: {
              gender = 'Nữ';
              break;
            }
            case 1: {
              gender = 'Nam';
              break;
            }
            case 2: {
              gender = 'Khác';
              break;
            }
          }
          let position: Position = new Position();
          if (employee.position) {
            position = Positions.find((p) => p.value === employee.position)!;
          }
          this.data.push({
            id: employee.employeeId,
            listCol: [
              String(index + 1),
              employee.fullName || '',
              this.datePipe.transform(employee.dob, 'dd/MM/yyyy') || '',
              gender || '',
              employee.email || '',
              employee.phone || '',
              employee.department?.name || '',
              position.text,
              this.datePipe.transform(employee.startDate, 'dd/MM/yyyy') || '',
            ],
          });
        });
        this.count = data.length;
      }
    });
  }
  add() {
    const modal = this.modal.create({
      nzTitle: 'Thêm mới nhân viên',
      nzContent: AddComponent,
      nzWidth: '80vw',
    });
    modal.afterClose.subscribe((data) => {
      if (data) {
        this.getAll();
      }
    });
  }

  handleDeleteEmit(id: any) {
    const confirm = this.modal.confirm({
      nzTitle: 'Bạn có muốn xóa nhân viên này không?',
      nzContent:
        'Nếu xóa nhân viên sẽ tự động được xóa khỏi phòng ban và tài khoản cũng tự động bị xóa khỏi hệ thống',
      nzOkText: 'Xóa',
      nzCancelText: 'Hủy',
      nzOnOk: () => {
        this.employeeService.delete(id).subscribe(
          (data) => {
            this.noti.successNoti('Xóa nhân viên thành công');
            this.getAll();
          },
          (err) => {
            this.noti.warningNoti();
          }
        );
      },
    });
  }

  handleEmitIsSave(e: any) {
    if (e) {
      this.getAll();
    }
  }
}
