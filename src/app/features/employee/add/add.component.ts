import { Template } from '@angular/compiler/src/render3/r3_ast';
import {
  Component,
  ContentChild,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { appearance } from 'src/app/core/const/material';
import { Positions } from 'src/app/core/const/position';
import { AccountService } from 'src/app/core/services/account.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { ValidatorService } from 'src/app/core/shared/validators/validator.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddComponent implements OnInit {
  appearance = appearance;
  Positions = Positions;
  username: string = '';
  pw: string = '';
  roleList = [
    { value: 0, text: 'Admin' },
    { value: 1, text: 'User' },
  ];
  form: FormGroup = this.fb.group(
    {
      name: ['', Validators.required],
      position: ['', Validators.required],
      role: ['', Validators.required],
      pw: ['', [Validators.required, Validators.minLength(6)]],
      rePw: ['', Validators.required],
      username: ['', Validators.required],
    },
    {
      validators: [this.validatorService.matchPasswordsValidator('pw', 'rePw')],
    }
  );

  constructor(
    private fb: FormBuilder,
    private validatorService: ValidatorService,
    private modalRef: NzModalRef,
    private accountService: AccountService,
    private noti: NotiService,
    private nzModal: NzModalService
  ) {}

  ngOnInit(): void {}

  handleEmitSelected(e: any) {
    if (e) {
      let body = {
        username: this.form.get('username')?.value,
        password: this.form.get('pw')?.value,
        fullname: this.form.get('name')?.value,
        position: this.form.get('position')?.value,
        role: this.form.get('role')?.value,
      };
      this.accountService.createAccForEmp(body).subscribe(
        (data) => {
          this.username = body.username;
          this.pw = body.password;
          this.noti.successNoti(
            `Tạo tài khoản thành công, Tài khoản: ${body.username}, Mật khẩu: ${body.password}`,
            5000
          );
          this.modalRef.close(true);
        },
        (err) => {
          this.noti.warningNoti(err.error);
        }
      );
    } else {
      this.modalRef.close(false);
    }
  }
}
