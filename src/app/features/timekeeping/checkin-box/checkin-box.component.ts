import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DateService } from 'src/app/core/services/date.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { TimekeepingService } from 'src/app/core/services/timekeeping.service';

@Component({
  selector: 'app-checkin-box',
  templateUrl: './checkin-box.component.html',
  styleUrls: ['./checkin-box.component.scss'],
})
export class CheckinBoxComponent implements OnInit {
  currentTime = new Date(Date.now());
  constructor(
    public dialogRef: MatDialogRef<CheckinBoxComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private timekeepingService: TimekeepingService,
    private noti: NotiService,
    private dateService: DateService
  ) {}

  ngOnInit(): void {
    setInterval(() => {
      this.currentTime = new Date(Date.now());
    }, 1000);
  }
  close() {
    this.dialogRef.close(false);
  }

  checkin() {
    let body = {
      date: new Date(Date.now()).toISOString(),
      timeCheckin: new Date(Date.now()).toISOString(),
      employeeId: this.data.empId,
    };
    this.timekeepingService.create(body).subscribe(
      (data) => {
        this.noti.successNoti('Vào ca thành công');
        this.dialogRef.close(true);
      },
      (err) => {
        this.noti.warningNoti();
      }
    );
  }
  checkout() {
    let body = {
      timeKeepingId: this.data.timekeeping.id,
      date: new Date(this.data.timekeeping.date).toISOString(),
      timeCheckin: this.data.timekeeping.in,
      timeCheckout: new Date(Date.now()).toISOString(),
      employeeId: this.data.timekeeping.empId,
    };
    this.timekeepingService.update(body).subscribe(
      (data) => {
        this.noti.successNoti('Tan ca thành công');
        this.dialogRef.close(true);
      },
      (err) => {
        this.noti.warningNoti();
      }
    );
  }
}
