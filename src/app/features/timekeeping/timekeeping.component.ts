import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NzCalendarMode } from 'ng-zorro-antd/calendar';
import { TimeKeeping } from 'src/app/core/models/timekeeping';
import { TimekeepingService } from 'src/app/core/services/timekeeping.service';
import { TimekeepingsettingService } from 'src/app/core/services/timekeepingsetting.service';
import { CheckinBoxComponent } from './checkin-box/checkin-box.component';
import { TimekeepingSettingComponent } from './timekeeping-setting/timekeeping-setting.component';

@Component({
  selector: 'app-timekeeping',
  templateUrl: './timekeeping.component.html',
  styleUrls: ['./timekeeping.component.scss'],
})
export class TimekeepingComponent implements OnInit {
  dateNow = new Date(Date.now());
  mode: NzCalendarMode = 'month';
  empId: string = '';
  role: string = 'User';
  constructor(
    private dialog: MatDialog,
    private timekeepingService: TimekeepingService,
    private tkps: TimekeepingsettingService,
    private datePipe: DatePipe
  ) {}
  listMapData: Checkin[] = [];
  timeIn: string = '';
  ngOnInit() {
    this.role = localStorage.getItem('role')!;
    this.empId = localStorage.getItem('id')!;
    this.getDetail();
    this.getTimeIn();
  }
  private getTimeIn() {
    this.tkps.get().subscribe((data) => {
      if (data) {
        this.timeIn = data.hourDefault + ':' + data.minuteDefault;
      }
    });
  }

  getDetail() {
    this.timekeepingService
      .getDetail(
        this.dateNow.getMonth() + 1,
        this.dateNow.getFullYear(),
        this.empId
      )
      .subscribe(
        (data: TimeKeeping[]) => {
          if (data) {
            this.mapData(data);
          }
        },
        (err) => {
          this.create();
        }
      );
  }

  create() {
    this.tkps
      .create({
        hourDefault: 7,
        minuteDefault: 30,
        modifiedTime: Date.now(),
      })
      .subscribe((data) => {
        this.getDetail();
      });
  }
  getDateOfMapList(date: number) {
    return this.listMapData.find((data) => {
      return data.date?.getDate() === date;
    });
  }

  mapData(data: TimeKeeping[]) {
    this.listMapData = data.map((t: TimeKeeping) => {
      return {
        id: t.timeKeepingId,
        status: t.status,
        isCheckin: t.isCheckin,
        isCheckout: t.isCheckout,
        in: t.timeCheckin ? t.timeCheckin : '',
        out: t.timeCheckout ? t.timeCheckout : '',
        date: new Date(t.date),
        empId: t.employeeId,
      };
    });
  }
  selectChange($event: Date, type: number, data: Checkin) {
    //type:  1: in // 2: out
    if (
      $event.getDate() === this.dateNow.getDate() &&
      $event.getMonth() === this.dateNow.getMonth()
    ) {
      const dialog = this.dialog.open(CheckinBoxComponent, {
        width: '400px',
        data: {
          type: type,
          timekeeping: data,
          empId: this.empId,
        },
      });
      dialog.afterClosed().subscribe((data) => {
        if (data) {
          this.getDetail();
        }
      });
    }
  }
  panelChange(change: { date: Date; mode: string }): void {
    console.log(change.date, change.mode);
  }

  setting() {
    const data = this.dialog.open(TimekeepingSettingComponent);
    data.afterClosed().subscribe((data) => {
      if (data) {
        this.getTimeIn();
      }
    });
  }
}

export class Checkin {
  id!: string;
  status?: boolean;
  isCheckin!: boolean;
  isCheckout!: boolean;
  in?: string;
  out?: string;
  date?: Date;
  empId!: string;
}
