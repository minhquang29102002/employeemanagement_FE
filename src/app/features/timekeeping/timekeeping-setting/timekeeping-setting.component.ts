import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { TKPS } from 'src/app/core/models/timekeepingsetting';
import { NotiService } from 'src/app/core/services/noti.service';
import { TimekeepingsettingService } from 'src/app/core/services/timekeepingsetting.service';

@Component({
  selector: 'app-timekeeping-setting',
  templateUrl: './timekeeping-setting.component.html',
  styleUrls: ['./timekeeping-setting.component.scss'],
})
export class TimekeepingSettingComponent implements OnInit {
  hours = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23,
  ];
  minutes = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58,
    59,
  ];
  form: FormGroup = this.fb.group({
    hour: [''],
    minute: [''],
  });
  setting: TKPS = new TKPS();
  constructor(
    private dialogRef: MatDialogRef<TimekeepingSettingComponent>,
    private tkpsService: TimekeepingsettingService,
    private fb: FormBuilder,
    private noti: NotiService
  ) {}

  ngOnInit(): void {
    this.getDetail();
  }
  private getDetail() {
    this.tkpsService.get().subscribe(
      (data: TKPS) => {
        if (data) {
          this.setting = data;
          this.form.patchValue({
            hour: data.hourDefault,
            minute: data.minuteDefault,
          });
        }
      },
      (err) => {
        this.create();
      }
    );
  }
  create() {
    this.tkpsService
      .create({
        hourDefault: 7,
        minuteDefault: 30,
        modifiedTime: Date.now(),
      })
      .subscribe((data) => {
        this.getDetail();
      });
  }
  handleEmitSelected($event: boolean) {
    if ($event) {
      let body = {
        id: this.setting.id,
        hourDefault: this.form.get('hour')?.value,
        minuteDefault: this.form.get('minute')?.value,
      };
      this.tkpsService.update(body).subscribe(
        (data) => {
          this.noti.successNoti();
          this.dialogRef.close(true);
        },
        (err) => {
          this.noti.warningNoti();
        }
      );
    } else {
      this.dialogRef.close();
    }
  }
}
