import { Routes, RouterModule } from '@angular/router';
import { TimekeepingComponent } from './timekeeping.component';

const routes: Routes = [
  {
    path: '',
    component: TimekeepingComponent,
  },
];

export const TimekeepingRoutes = RouterModule.forChild(routes);
