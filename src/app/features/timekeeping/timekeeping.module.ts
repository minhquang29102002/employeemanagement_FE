import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimekeepingComponent } from './timekeeping.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { TimekeepingRoutes } from './timekeeping.routing';
import { CheckinBoxComponent } from './checkin-box/checkin-box.component';
import { TimekeepingSettingComponent } from './timekeeping-setting/timekeeping-setting.component';

@NgModule({
  imports: [CommonModule, TimekeepingRoutes, SharedModule],
  declarations: [TimekeepingComponent, CheckinBoxComponent, TimekeepingSettingComponent],
})
export class TimekeepingModule {}
