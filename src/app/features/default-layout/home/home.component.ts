import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Position, Positions } from 'src/app/core/const/position';
import { TimekeepingService } from 'src/app/core/services/timekeeping.service';
import { ListRow } from 'src/app/core/shared/components/table/table.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  headerList: string[] = [
    'Tên',
    'Phòng ban',
    'Vị trí',
    'Giờ vào',
    'Giờ ra',
    'Trạng thái',
  ];
  data: ListRow[] = [];
  constructor(
    private tkpService: TimekeepingService,
    private datePipe: DatePipe
  ) {}

  ngOnInit(): void {
    this.tkpService.getReport().subscribe((data) => {
      if (data) {
        data.forEach((element: any) => {
          let status = '';
          switch (element.status) {
            case true: {
              status = 'Đúng giờ';
              break;
            }
            case false: {
              status = 'Trễ giờ';
              break;
            }
            case null: {
              status = 'Chưa chấm công';
              break;
            }
          }
          let position: Position = new Position();
          if (element.position) {
            position = Positions.find((p) => p.value === element.position)!;
          }
          this.data.push({
            listCol: [
              element.employeeName,
              element.department?.name,
              position.text,
              this.datePipe.transform(element.timeCheckin, 'hh:mm '),
              this.datePipe.transform(element.timeCheckout, 'hh:mm '),
              status,
            ],
          });
        });
      }
    });
  }
}
