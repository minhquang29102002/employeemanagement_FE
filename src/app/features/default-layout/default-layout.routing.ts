import { Routes, RouterModule } from '@angular/router';
import { TrashComponent } from '../trash/trash.component';
import { DefaultLayoutComponent } from './default-layout.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'timekeeping',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'department',
        loadChildren: () =>
          import('./../department/department.module').then(
            (m) => m.DepartmentModule
          ),
      },
      {
        path: 'employee',
        loadChildren: () =>
          import('./../employee/employee.module').then((m) => m.EmployeeModule),
      },
      {
        path: 'timekeeping',
        loadChildren: () =>
          import('./../timekeeping/timekeeping.module').then(
            (m) => m.TimekeepingModule
          ),
      },
      {
        path: 'trash',
        component: TrashComponent,
      },
    ],
  },
];

export const DefaultLayoutRoutes = RouterModule.forChild(routes);
