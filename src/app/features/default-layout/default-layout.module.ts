import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { DefaultLayoutComponent } from './default-layout.component';
import { DefaultLayoutRoutes } from './default-layout.routing';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [SidebarComponent, DefaultLayoutComponent, HomeComponent],
  imports: [CommonModule, SharedModule, DefaultLayoutRoutes],
})
export class DefaultLayoutModule {}
