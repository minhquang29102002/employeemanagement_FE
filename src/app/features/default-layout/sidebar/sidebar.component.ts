import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  isCollapse: boolean = false;
  sideBarUser: Sidebar[] = [
    {
      text: 'Chấm công',
      icon: '<i class="fa-regular fa-calendar-check"></i>',
      navigate: '/main/timekeeping',
    },
  ];
  sideBarAdmin: Sidebar[] = [
    {
      text: 'Chấm công',
      icon: '<i class="fa-regular fa-calendar-check"></i>',
      navigate: '/main/timekeeping',
    },
    {
      text: 'Báo cáo',
      icon: '<i class="fa-solid fa-house"></i>',
      navigate: '/main/home',
    },
    {
      text: 'Phòng ban',
      navigate: '/main/department',
      icon: '<i class="fa-regular fa-building"></i>',
    },
    {
      text: 'Nhân sự',
      navigate: '/main/employee',
      icon: '<i class="fa-solid fa-users"></i>',
    },
  ];
  role: string = '';
  constructor() {}

  ngOnInit(): void {
    this.role = localStorage.getItem('role')!;
    window.addEventListener('scroll', function () {
      var element = document.querySelector('.sidebar');
      var scrollPosition = window.scrollY;
      console.log(123);
      if (scrollPosition > window.innerHeight) {
        element!.classList.add('h-full');
      } else {
        element!.classList.remove('h-full');
      }
    });
  }
  toggleCollapse() {
    this.isCollapse = !this.isCollapse;
  }
}
export class Sidebar {
  icon?: string;
  text?: string;
  navigate?: string;
}
