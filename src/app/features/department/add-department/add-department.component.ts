import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { appearance } from 'src/app/core/const/material';
import { Employee } from 'src/app/core/models/employee';
import { DepartmentService } from 'src/app/core/services/department.service';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { NotiService } from 'src/app/core/services/noti.service';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.scss'],
})
export class AddDepartmentComponent implements OnInit {
  employeeList: Employee[] = [];
  form: FormGroup = this.fb.group({
    name: ['', Validators.required],
    des: [''],
    emp: [''],
  });
  appearance = appearance;
  constructor(
    private employeeService: EmployeeService,
    private fb: FormBuilder,
    private modal: NzModalRef,
    private noti: NotiService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit(): void {
    this.employeeService
      .getEmployeeNotInDepartment()
      .subscribe((data: Employee[]) => {
        if (data) {
          this.employeeList = data;
        }
      });
  }

  handleEmitSelected(e: boolean) {
    if (e) {
      let body = {
        name: this.form.get('name')?.value,
        description: this.form.get('des')?.value,
        listEmployee: this.form.get('emp')?.value,
      };
      this.departmentService.create(body).subscribe(
        (data) => {
          this.noti.successNoti('Tạo phòng ban thành công');
          this.modal.close(true);
        },
        (err) => {
          this.noti.warningNoti();
        }
      );
    } else {
      this.modal.close(false);
    }
  }
}
