import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { Department } from 'src/app/core/models/department';
import { DepartmentService } from 'src/app/core/services/department.service';
import { NotiService } from 'src/app/core/services/noti.service';
import {
  ListRow,
  Screen,
} from 'src/app/core/shared/components/table/table.component';
import { AddDepartmentComponent } from './add-department/add-department.component';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss'],
})
export class DepartmentComponent implements OnInit {
  headerList = [
    'STT',
    'Tên phòng ban',
    'Số lượng nhân viên',
    'Mô tả',
    'Hành động',
  ];
  data: ListRow[] = [];
  count: number = 0;
  Screen = Screen;
  constructor(
    private departmentService: DepartmentService,
    private nzModel: NzModalService,
    private noti: NotiService
  ) {}

  ngOnInit() {
    this.getAll();
  }
  getAll() {
    this.data = [];
    this.departmentService.getAll('All').subscribe((data: Department[]) => {
      if (data) {
        this.count = data.length;
        data.forEach((item: Department, index) => {
          this.data.push({
            id: item.departmentId,
            listCol: [
              String(index + 1),
              item.name || '',
              String(item.totalEmployee) || '0',
              item.description || '',
            ],
          });
        });
      }
    });
  }

  search($event: any) {
    this.data = [];
    var keyword = 'All';
    if (
      $event.target.value.trim() === '' ||
      $event.target.value.trim() === null
    ) {
      keyword = 'All';
    } else {
      keyword = $event.target.value.trim();
    }
    this.departmentService.getAll(keyword).subscribe((data: Department[]) => {
      if (data) {
        this.count = data.length;
        data.forEach((item: Department, index) => {
          this.data.push({
            id: item.departmentId,
            listCol: [
              String(index + 1),
              item.name || '',
              String(item.totalEmployee) || '0',
              item.description || '',
            ],
          });
        });
      }
    });
  }
  add() {
    const modalRef = this.nzModel.create({
      nzTitle: 'Thêm mới phòng ban',
      nzContent: AddDepartmentComponent,
    });
    modalRef.afterClose.subscribe((isCreateSucessful) => {
      if (isCreateSucessful) {
        this.getAll();
      }
    });
  }

  handleSaveEmit($event: boolean) {
    if ($event) {
      this.getAll();
    }
  }

  handleDeleteEmit(id: string) {
    const confirm = this.nzModel.confirm({
      nzTitle: 'Bạn có muốn xóa phòng ban này không?',
      nzOkText: 'Xóa',
      nzCancelText: 'Hủy',
      nzOnOk: () => {
        this.departmentService.delete(id).subscribe(
          (data) => {
            this.noti.successNoti('Xóa phòng ban thành công');
            this.getAll();
          },
          (err) => {
            this.noti.warningNoti();
          }
        );
      },
    });
  }
}
