import { DatePipe } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NzModalService } from 'ng-zorro-antd/modal';
import { appearance } from 'src/app/core/const/material';
import { Positions } from 'src/app/core/const/position';
import { Department } from 'src/app/core/models/department';
import { Employee } from 'src/app/core/models/employee';
import { DepartmentService } from 'src/app/core/services/department.service';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { ListRow } from 'src/app/core/shared/components/table/table.component';

@Component({
  selector: 'app-detail-department',
  templateUrl: './detail-department.component.html',
  styleUrls: ['./detail-department.component.scss'],
})
export class DetailDepartmentComponent implements OnInit {
  department: Department = new Department();
  title: string = 'Xem chi tiết phòng ban';
  isEdit: boolean = false;
  appearance = appearance;
  headerList: string[] = [
    'STT',
    'Họ tên',
    'Ngày sinh',
    'Giới tính',
    'Số điện thoại',
    'Email',
    'Vị trí',
    'Hành động',
  ];
  listEmp: ListRow[] = [];
  listEmployeeBeforeMap: Employee[] = [];
  form: FormGroup = this.fb.group({
    name: '',
    desc: '',
    addEmployee: null,
  });
  employeeNotInDepartment: Employee[] = [];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private departmentService: DepartmentService,
    private fb: FormBuilder,
    private datePipe: DatePipe,
    public dialogRef: MatDialogRef<DetailDepartmentComponent>,
    private employeeService: EmployeeService,
    private noti: NotiService,
    private nzModel: NzModalService
  ) {}

  ngOnInit(): void {
    this.getDetailDepartment();
    this.getListEmployeeNotInDepartment();
  }

  private getDetailDepartment() {
    this.departmentService
      .getById(this.data.id)
      .subscribe((data: Department) => {
        this.department = data;
        this.form.patchValue({
          name: this.department.name,
          desc: this.department.description,
        });
        if (data.employeeList) {
          this.listEmployeeBeforeMap = data.employeeList;
          this.mapEmployeeList();
        }
      });
  }

  private mapEmployeeList() {
    this.listEmp = [];
    this.listEmployeeBeforeMap.forEach((emp: Employee, index: number) => {
      let postion = '';
      if (emp.position) {
        postion = Positions[emp.position].text;
      }
      let gender = '';
      switch (emp.gender) {
        case 0: {
          gender = 'Nữ';
          break;
        }
        case 1: {
          gender = 'Nam';
          break;
        }
        case 2: {
          gender = 'Khác';
          break;
        }
      }
      this.listEmp.push({
        id: emp.employeeId,
        listCol: [
          index + 1,
          emp.fullName,
          this.datePipe.transform(emp.dob, 'dd/MM/yyyy'),
          gender,
          emp.phone,
          emp.email,
          postion,
        ],
      });
    });
  }

  close() {
    this.dialogRef.close(false);
  }

  delete(row: ListRow) {
    this.listEmp.splice(this.listEmp.indexOf(row), 1);
    this.listEmployeeBeforeMap = this.listEmployeeBeforeMap.filter((e) => {
      return e.employeeId != row.id;
    });
  }

  getListEmployeeNotInDepartment() {
    this.employeeService.getEmployeeNotInDepartment().subscribe((data) => {
      if (data) {
        this.employeeNotInDepartment = data;
      }
    });
  }

  save() {
    let body = {
      id: this.data.id,
      name: this.form.get('name')?.value,
      description: this.form.get('desc')?.value,
      listEmployee: this.listEmployeeBeforeMap.map((e) => {
        return e.employeeId;
      }),
    };
    console.log(body);
    this.departmentService.update(body).subscribe(
      (data) => {
        this.noti.successNoti();
        this.dialogRef.close(true);
      },
      (err) => {
        this.noti.warningNoti();
      }
    );
  }

  change() {
    this.listEmployeeBeforeMap = [
      ...this.listEmployeeBeforeMap,
      this.form.get('addEmployee')?.value,
    ];
    this.employeeNotInDepartment.splice(
      this.employeeNotInDepartment.indexOf(this.form.get('addEmployee')?.value),
      1
    );
    this.mapEmployeeList();
  }

  deleteDepartment() {
    const confirm = this.nzModel.confirm({
      nzTitle: 'Bạn có muốn xóa phòng ban này không?',
      nzOkText: 'Xóa',
      nzCancelText: 'Hủy',
      nzOnOk: () => {
        this.departmentService.delete(this.data.id).subscribe(
          (data) => {
            this.noti.successNoti('Xóa phòng ban thành công');
            this.dialogRef.close(true);
          },
          (err) => {
            this.noti.warningNoti();
          }
        );
      },
    });
  }
}
