import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentComponent } from './department.component';
import { DepartmentRoutes } from './department.routing';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { AddDepartmentComponent } from './add-department/add-department.component';
import { DetailDepartmentComponent } from './detail-department/detail-department.component';

@NgModule({
  imports: [CommonModule, DepartmentRoutes, SharedModule],
  declarations: [
    DepartmentComponent,
    AddDepartmentComponent,
    DetailDepartmentComponent,
  ],
})
export class DepartmentModule {}
