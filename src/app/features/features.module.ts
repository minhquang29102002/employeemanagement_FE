import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultLayoutComponent } from './default-layout/default-layout.component';
import { DepartmentComponent } from './department/department.component';
import { TrashComponent } from './trash/trash.component';

@NgModule({
  declarations: [
    TrashComponent
  ],
  imports: [CommonModule],
})
export class FeaturesModule {}
