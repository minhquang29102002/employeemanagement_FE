import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../const/urls';

@Injectable({
  providedIn: 'root',
})
export class DepartmentService {
  constructor(private http: HttpClient) {}
  API = API_URL + '/Department';
  getAll(keyword: string): Observable<any> {
    return this.http.get(this.API + '/GetAll?keyword=' + keyword);
  }
  create(body: any): Observable<any> {
    return this.http.post(this.API + '/Create', body);
  }
  getById(id: string): Observable<any> {
    return this.http.get(this.API + '/GetById?Id=' + id);
  }
  update(body: any): Observable<any> {
    return this.http.post(this.API + '/Update', body);
  }
  delete(id: string): Observable<any> {
    return this.http.delete(this.API + '/Delete?Id=' + id);
  }
}
