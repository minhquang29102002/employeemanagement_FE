import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../const/urls';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  empAPI = API_URL + '/Employee';
  constructor(private http: HttpClient) {}
  getAll(keyword: string): Observable<any> {
    return this.http.get(this.empAPI + '/GetAllEmployee?keyword=' + keyword);
  }
  getEmployeeNotInDepartment(): Observable<any> {
    return this.http.get(this.empAPI + '/GetEmployeeNotInDepartment');
  }
  getById(id: string): Observable<any> {
    return this.http.get(this.empAPI + '/GetById?id=' + id);
  }
  update(body: any): Observable<any> {
    return this.http.patch(this.empAPI + '/Update', body);
  }
  delete(id: string): Observable<any> {
    return this.http.delete(this.empAPI + '/DeleteEmployee?id=' + id);
  }
}
