import { Injectable } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';

@Injectable({
  providedIn: 'root',
})
export class NotiService {
  constructor(private message: NzMessageService) {}
  successNoti(message?: string, duration?: number) {
    this.message.create('loading', 'Loading...', {
      nzDuration: 500,
    });
    setTimeout(() => {
      this.message.create('success', message || 'Lưu thông tin thành công', {
        nzDuration: duration || 3000,
      });
    }, 700);
  }

  warningNoti(message?: string) {
    this.message.create('warning', message || 'Có lỗi xảy ra');
  }
}
