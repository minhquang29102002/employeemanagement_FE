import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../const/urls';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  accountApi: string = API_URL + '/Account';
  constructor(private http: HttpClient) {}
  create(body: any): Observable<any> {
    return this.http.post(this.accountApi + '/create', body);
  }
  signin(body: any): Observable<any> {
    return this.http.post(this.accountApi + '/SignIn', body);
  }
  changePw(body: any): Observable<any> {
    return this.http.post(this.accountApi + '/ChangePw', body);
  }
  createAccForEmp(body: any): Observable<any> {
    return this.http.post(this.accountApi + '/CreateForEmp', body);
  }
  forgotPw(body: any): Observable<any> {
    return this.http.post(this.accountApi + '/ForgotPw', body);
  }
}
