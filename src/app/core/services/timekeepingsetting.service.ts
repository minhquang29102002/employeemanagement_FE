import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../const/urls';

@Injectable({
  providedIn: 'root',
})
export class TimekeepingsettingService {
  API = API_URL + '/TimekeepingSetting';
  constructor(private http: HttpClient) {}
  update(body: any): Observable<any> {
    return this.http.post(this.API + '/Update', body);
  }
  get(): Observable<any> {
    return this.http.get(this.API + '/Get');
  }
  create(body: any): Observable<any> {
    return this.http.post(this.API + '/Create', body);
  }
}
