import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from '../const/urls';

@Injectable({
  providedIn: 'root',
})
export class TimekeepingService {
  API = API_URL + '/TimeKeeping';
  constructor(private http: HttpClient) {}
  getDetail(month: number, year: number, employeeId: string): Observable<any> {
    return this.http.get(
      this.API +
        '/GetDetail?iMonth=' +
        month +
        '&iYear=' +
        year +
        '&iEmployeeId=' +
        employeeId
    );
  }

  update(body: any): Observable<any> {
    return this.http.post(this.API + '/Update', body);
  }

  create(body: any): Observable<any> {
    return this.http.post(this.API + '/Create', body);
  }

  getReport(): Observable<any> {
    return this.http.get(this.API + '/GetReport');
  }
}
