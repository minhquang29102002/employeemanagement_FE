import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {
  constructor() {}
  getDateNow() {
    var d = new Date();
    d.setHours(0, -d.getTimezoneOffset(), 0, 0); //removing the timezone offset.
    return d.toISOString();
  }
}
