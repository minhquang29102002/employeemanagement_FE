import { MatFormFieldAppearance } from '@angular/material/form-field';

export const avavarDefault = './../../../../assets/avatar2.png';
export const backgroundLogin = './../../../../assets/image-login.jpg';
export const logo = './../../../../assets/logo.png';
export const googleIcon = './../../../../assets/google.png';
export const appearance: MatFormFieldAppearance = 'outline';
