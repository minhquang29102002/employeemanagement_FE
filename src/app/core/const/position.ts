export const Positions: Position[] = [
  {
    value: 1,
    text: 'Frontend',
  },
  {
    value: 2,
    text: 'PM',
  },
  {
    value: 3,
    text: 'Tester',
  },
  {
    value: 4,
    text: 'Backend',
  },
  {
    value: 5,
    text: 'BA',
  },
];
export class Position {
  value!: number;
  text!: string;
}
