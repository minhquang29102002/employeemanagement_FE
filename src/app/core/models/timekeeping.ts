import { Employee } from './employee';

export interface TimeKeeping {
  timeKeepingId: string;
  date: string;
  status: boolean;
  isCheckin: boolean;
  isCheckout: boolean;
  timeCheckin: string;
  timeCheckout: any;
  employeeId: string;
  employee: Employee;
}
