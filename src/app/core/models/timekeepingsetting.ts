export class TKPS {
  id?: string;
  hourDefault?: number;
  minuteDefault?: number;
  modifiedTime?: string;
}
