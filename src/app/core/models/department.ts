import { Employee } from './employee';

export class Department {
  departmentId?: string;
  name?: string;
  totalEmployee?: number;
  description?: string;
  employeeList?: Employee[];
}
