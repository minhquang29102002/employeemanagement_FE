import { avavarDefault } from '../const/material';
import { Department } from './department';

export class Information {
  employeeId?: string;
  fullName?: string;
  gender?: any;
  dob?: any;
  phone?: string;
  cccd?: any;
  address?: any;
  avatar?: any;
  departmentId?: any;
  position?: any;
  email?: string;
  role?: number;
  department?: Department;
  constructor(init?: Partial<Information>) {
    if (init) {
      Object.assign(this, init);
    }
  }
}
