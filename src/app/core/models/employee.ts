import { Department } from './department';

export class Employee {
  employeeId?: string;
  fullName?: string;
  gender?: any;
  dob?: any;
  phone?: string;
  cccd?: any;
  address?: any;
  avatar?: any;
  position?: any;
  role?: any;
  email?: string;
  departmentId?: any;
  department?: Department;
  startDate!: string;
  timeKeepingList: any;
}
