import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ValidatorService {
  constructor() {}
  matchPasswordsValidator(
    passwordKey: string,
    confirmPasswordKey: string
  ): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const password = control.get(passwordKey);
      const confirmPassword = control.get(confirmPasswordKey);

      return password &&
        confirmPassword &&
        password.value !== confirmPassword.value
        ? { matchPasswords: true }
        : null;
    };
  }
}
