import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Information } from '../../models/information';
import { EmployeeService } from '../../services/employee.service';

@Injectable({
  providedIn: 'root',
})
export class CommonService {
  private informationSource = new BehaviorSubject<Information>({});
  private isHideSidebarSource = new BehaviorSubject<boolean>(false);
  information$ = this.informationSource.asObservable();
  isHideSidebar$ = this.isHideSidebarSource.asObservable();
  constructor(private employeeService: EmployeeService) {}
  setInformationSource(infor: Information) {
    this.informationSource.next(infor);
  }
  setIsHideSidebarSource(value: boolean) {
    this.isHideSidebarSource.next(value);
  }

  getInfomation() {
    this.employeeService
      .getById(localStorage.getItem('id')!)
      .subscribe((data: Information) => {
        this.informationSource.next(data);
      });
  }
}
