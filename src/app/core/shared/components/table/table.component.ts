import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DetailDepartmentComponent } from 'src/app/features/department/detail-department/detail-department.component';
import { ViewEditComponent } from 'src/app/features/employee/view-edit/view-edit.component';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() headerList: string[] = [];
  @Input() data: ListRow[] = [];
  @Input() screen!: Screen;
  @Output() isSave$: EventEmitter<boolean> = new EventEmitter();
  @Output() delete$: EventEmitter<string> = new EventEmitter();
  @Input() isShowEdit: boolean = true;
  constructor(private dialog: MatDialog) {}

  ngOnInit(): void {}
  edit(id: any) {
    switch (this.screen) {
      case Screen.DEPARTMENT: {
        const dialog = this.dialog.open(DetailDepartmentComponent, {
          height: 'fit-content',
          width: '80vw',
          data: {
            id: id,
          },
        });
        dialog.afterClosed().subscribe((data) => {
          if (data) {
            this.isSave$.emit(true);
          }
        });
        break;
      }
      case Screen.EMPLOYEE: {
        const dialog = this.dialog.open(ViewEditComponent, {
          height: 'fit-content',
          width: '80vw',
          data: {
            id: id,
          },
        });
        dialog.afterClosed().subscribe((data) => {
          if (data) {
            this.isSave$.emit(true);
          }
        });
        break;
      }
    }
  }
  delete(id: any) {
    this.delete$.emit(id);
  }
}
export class ListRow {
  id?: string;
  listCol?: any[];
}
export enum Screen {
  DEPARTMENT,
  EMPLOYEE,
}
