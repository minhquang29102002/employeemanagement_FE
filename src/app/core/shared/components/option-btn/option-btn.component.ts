import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-option-btn',
  templateUrl: './option-btn.component.html',
  styleUrls: ['./option-btn.component.scss'],
})
export class OptionBtnComponent implements OnInit {
  @Output() selected$: EventEmitter<boolean> = new EventEmitter();
  @Input() buttons: string[] = ['Hủy', 'Lưu'];
  @Input() isValid: boolean = false;
  constructor() {}

  ngOnInit(): void {}
  emit(value: boolean): void {
    this.selected$.emit(value);
  }
}
