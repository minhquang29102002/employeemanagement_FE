import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { JwksValidationHandler } from 'angular-oauth2-oidc-jwks';

@Injectable({
  providedIn: 'root',
})
export class GoogleAuthService {
  constructor(private oauthService: OAuthService) {
    this.configAuth();
  }
  logIn() {
    this.oauthService.initImplicitFlow();
  }

  configAuth() {
    this.oauthService.configure(authConfig);
    this.oauthService.logoutUrl = 'https://www.google.com/accounts/Logout';
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
}
