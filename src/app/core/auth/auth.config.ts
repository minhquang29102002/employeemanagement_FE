import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig: AuthConfig = {
  issuer: 'https://accounts.google.com',
  strictDiscoveryDocumentValidation: false,
  redirectUri: window.location.origin,
  clientId:
    '1036151430116-a4i081c8ri4tau1a6ct4qdn0shun2l2t.apps.googleusercontent.com',
  scope: 'openid profile email',
};
