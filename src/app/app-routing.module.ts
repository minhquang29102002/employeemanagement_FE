import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGaurdService } from './core/auth/auth-gaurd.service';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: () =>
      import('./layout/main/main.module').then((m) => m.MainModule),
    canActivate: [AuthGaurdService],
  },
  {
    path: '',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('./layout/auth/auth.module').then((m) => m.AuthModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
