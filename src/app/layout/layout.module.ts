import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../core/shared/shared.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, SharedModule],
})
export class LayoutModule {}
