import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { AccountService } from 'src/app/core/services/account.service';
import { NotiService } from 'src/app/core/services/noti.service';

@Component({
  selector: 'app-forgot-pw',
  templateUrl: './forgot-pw.component.html',
  styleUrls: ['./forgot-pw.component.scss'],
})
export class ForgotPwComponent implements OnInit {
  form: FormGroup = this.fb.group({
    username: ['', Validators.required],
    email: ['', Validators.required],
  });
  constructor(
    private accountService: AccountService,
    private dialogRef: MatDialogRef<ForgotPwComponent>,
    private fb: FormBuilder,
    private noti: NotiService
  ) {}

  ngOnInit(): void {}
  handleEmitSelected($event: boolean) {
    if ($event) {
      let body = {
        username: this.form.get('username')?.value,
        email: this.form.get('email')?.value,
      };
      this.accountService.forgotPw(body).subscribe(
        (data) => {
          if (data) {
            this.dialogRef.close(data.passWord);
          }
        },
        (err) => {
          this.noti.warningNoti(err.error);
        }
      );
    } else {
      this.dialogRef.close();
    }
  }
}
