import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { GoogleAuthService } from 'src/app/core/auth/google-auth.service';
import { backgroundLogin, googleIcon, logo } from 'src/app/core/const/material';
import { Employee } from 'src/app/core/models/employee';
import { AccountService } from 'src/app/core/services/account.service';
import { NotiService } from 'src/app/core/services/noti.service';
import { ForgotPwComponent } from '../forgot-pw/forgot-pw.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  backgroundLogin = backgroundLogin;
  logo = logo;
  userName!: string;
  passWord!: string;
  form: FormGroup = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });
  isLoading: boolean = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private accountService: AccountService,
    private noti: NotiService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {}

  logIn() {
    this.isLoading = true;
    let body = {
      username: this.form.get('username')?.value,
      password: this.form.get('password')?.value,
    };
    this.accountService.signin(body).subscribe(
      (data: Employee) => {
        this.isLoading = false;
        localStorage.setItem('id', data.employeeId!);
        localStorage.setItem('role', data.role === 0 ? 'Admin' : 'User');
        this.router.navigate(['/main']);
      },
      (err) => {
        this.isLoading = false;
        this.noti.warningNoti(err.error);
      }
    );
  }

  createAcc() {
    this.router.navigate(['/auth/signup']);
  }

  forgotPw() {
    const dialog = this.dialog.open(ForgotPwComponent, {
      width: '50vw',
    });
    dialog.afterClosed().subscribe((data) => {
      if (data) {
        this.noti.successNoti('Mật khẩu cũ của bạn là: ' + data, 10000);
      }
    });
  }
}
