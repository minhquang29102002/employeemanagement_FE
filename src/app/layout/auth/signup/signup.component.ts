import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { backgroundLogin, logo } from 'src/app/core/const/material';
import { AccountService } from 'src/app/core/services/account.service';
import { NotiService } from 'src/app/core/services/noti.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit {
  backgroundLogin = backgroundLogin;
  logo = logo;
  form: FormGroup = this.fb.group({
    username: ['', Validators.required],
    fullname: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    password: ['', Validators.required],
    repw: ['', Validators.required],
    role: ['', Validators.required],
  });
  listOfOption = [
    {
      label: 'Quản trị viên',
      value: 0,
    },
    {
      label: 'Nhân viên',
      value: 1,
    },
  ];
  Role = [
    {
      text: 'User',
      value: 1,
    },
    {
      text: 'Admin',
      value: 0,
    },
  ];
  constructor(
    private account: AccountService,
    private fb: FormBuilder,
    private noti: NotiService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  create() {
    let body = {
      username: this.form.get('username')?.value,
      phone: this.form.get('phone')?.value,
      email: this.form.get('email')?.value,
      password: this.form.get('password')?.value,
      fullname: this.form.get('fullname')?.value,
      role: this.form.get('role')?.value,
    };
    let noti = '';
    if (!this.form.valid) {
      noti = 'Bạn chưa nhập đủ các trường';
      if (this.form.get('email')?.hasError('email')) {
        noti += ' hoặc email nhập sai định dạng';
      }
      this.noti.warningNoti(noti);
    } else if (
      this.form.get('password')?.value != this.form.get('repw')?.value
    ) {
      noti = ' Nhập lại sai mật khẩu';
      this.noti.warningNoti(noti);
    } else {
      this.account.create(body).subscribe(
        (data) => {
          this.noti.successNoti(
            'Tạo tài khoản thành công, mời về trang đăng nhập'
          );
          this.router.navigate(['/auth/login']);
        },
        (err) => {
          this.noti.warningNoti(err.error);
        }
      );
    }
  }
}
