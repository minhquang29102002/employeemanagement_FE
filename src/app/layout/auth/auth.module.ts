import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { AuthRoutes } from './auth.routing';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { ForgotPwComponent } from './forgot-pw/forgot-pw.component';

@NgModule({
  imports: [CommonModule, AuthRoutes, SharedModule],
  declarations: [
    AuthComponent,
    LoginComponent,
    SignupComponent,
    ForgotPwComponent,
  ],
})
export class AuthModule {}
