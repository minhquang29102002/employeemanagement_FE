import { AfterViewInit, Component, OnInit } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { avavarDefault, logo } from 'src/app/core/const/material';
import { Information } from 'src/app/core/models/information';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { CommonService } from 'src/app/core/shared/services/common.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss'],
})
export class MainHeaderComponent implements OnInit, AfterViewInit {
  avatar: string = avavarDefault;
  information$ = this.commonService.information$;
  logo = logo;
  constructor(
    private employeeService: EmployeeService,
    private commonService: CommonService
  ) {}

  ngOnInit(): void {}
  ngAfterViewInit(): void {
    this.commonService.getInfomation();
  }

  navigateToHome() {
    this.commonService.setIsHideSidebarSource(false);
  }
}
