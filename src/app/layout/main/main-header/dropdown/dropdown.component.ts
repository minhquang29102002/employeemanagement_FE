import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { avavarDefault } from 'src/app/core/const/material';
import { Information } from 'src/app/core/models/information';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
})
export class DropdownComponent implements OnInit {
  @Input() information: Information = new Information({});
  avatarDefault: string = avavarDefault;
  constructor(private router: Router) {}

  ngOnInit(): void {}

  logOut() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }
}
