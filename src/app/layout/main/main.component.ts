import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { CommonService } from 'src/app/core/shared/services/common.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  isHideSidebar$ = this.commonService.isHideSidebar$;

  constructor(
    private commonService: CommonService,
    private employeeService: EmployeeService
  ) {}

  ngOnInit() {
    setTimeout(() => {
      this.commonService.setIsHideSidebarSource(false);
    });
  }
}
