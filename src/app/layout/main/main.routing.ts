import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./../../features/default-layout/default-layout.module').then(
            (m) => m.DefaultLayoutModule
          ),
      },
      {
        path: 'setting',
        loadChildren: () =>
          import('./../../features/settings/settings.module').then(
            (m) => m.SettingsModule
          ),
      },
    ],
  },
];

export const MainRoutes = RouterModule.forChild(routes);
