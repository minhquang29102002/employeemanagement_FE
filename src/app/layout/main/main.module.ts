import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/core/shared/shared.module';
import { MainFooterComponent } from './main-footer/main-footer.component';
import { DropdownComponent } from './main-header/dropdown/dropdown.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { MainComponent } from './main.component';
import { MainRoutes } from './main.routing';

@NgModule({
  imports: [CommonModule, MainRoutes, SharedModule],
  declarations: [
    MainComponent,
    MainHeaderComponent,
    MainFooterComponent,
    DropdownComponent,
  ],
})
export class MainModule {}
