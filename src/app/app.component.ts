import { Component } from '@angular/core';
import { IconService } from '@ant-design/icons-angular';
import { OAuthService } from 'angular-oauth2-oidc';
import { GoogleAuthService } from './core/auth/google-auth.service';
import { AccountBookFill } from '@ant-design/icons-angular/icons';
import { initializeApp } from 'firebase/app';
import { getAnalytics } from 'firebase/analytics';
const firebaseConfig = {
  apiKey: 'AIzaSyDsja3NP3dNJqHBYJzhvt9wUivrXKiE2wA',
  authDomain: 'employeemanagement-790a2.firebaseapp.com',
  projectId: 'employeemanagement-790a2',
  storageBucket: 'employeemanagement-790a2.appspot.com',
  messagingSenderId: '1030751654205',
  appId: '1:1030751654205:web:135e596a7a37a9c421f554',
  measurementId: 'G-VTP5NTSDSM',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'EmployeeManagement';
  constructor() {}
}
